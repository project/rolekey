<?php

/**
 * @file
 * Contains all theme related functions.
 */

/**
 * Themes the rolekey permissions form
 */
function theme_rolekey_list_key_roles($form) {
  $rows = array();
  foreach (element_children($form['roles']) as $key) {
    if (is_array($form['roles'][$key])) {
      $row = array();
      $row[] = array('data' => drupal_render($form['roles'][$key]));
      foreach (element_children($form['checkboxes']) as $rid) {
        if (is_array($form['checkboxes'][$rid])) {
          $row[] = array('data' => drupal_render($form['checkboxes'][$rid][$key]), 'align' => 'left', 'title' => t($key));
        }
      }

      $rows[] = $row;
    }
  }
  $columns = (empty($rows)) ? 2 : count(current($rows));
  $disabled_message = t('This role is automatically assigned to all users.');
  $disabled_message_cell = ($columns == 2) ? array('data' => $disabled_message) :  array('data' => $disabled_message, 'colspan' => ($columns - 1));
  // Give the users the standard message about authenticated user role
  $row = array(array('data' => t('Authenticated user')), $disabled_message_cell);
  array_unshift($rows, $row);

  $header[] = (t('Roles'));
  foreach (element_children($form['role_names']) as $rid) {
    if (is_array($form['role_names'][$rid])) {
      $header[] = drupal_render($form['role_names'][$rid]);
    }
  }
  $output = theme('table', $header, $rows, array('id' => 'roles'));
  $output .= drupal_render($form);
  return $output;
}

/**
 * Allows the user to theme the membership notices.
 */
function theme_rolekey_user_membership_notice($memberships) {
  if (count($memberships)) {
    $output = t('You are a member of the following groups');
    $items = array();
    foreach ($memberships as $m) {
      $items[] = t($m);
    }
    $output .= theme('item_list', $items);
    return $output;
  }
  else {
    return t('You do not belong to any groups.');
  }
}

/**
 * This themes the main table for the registration role keys
 *
 * @param array $form
 * @return string Formatted HTML form
 */
function theme_rolekey_list_keys_form($form) {
  $header = array(array('data' => t('Name'), 'colspan' => 1), t('Key'), t('Status'), t('Operations'));
  foreach (rolekey_keys(TRUE) as $kid => $key) {
    $name = $key->name;
    $key_code = empty($key->rolekey) ? '' : $key->rolekey;
    $status = drupal_render($form[$kid]['status']);
    $rows[] = array(array('data' => $name, 'colspan' => 1), $key_code, $status, l(t('edit'), 'admin/user/rolekey/edit/'. $kid));
  }
  $rows[] = array(array('data' => drupal_render($form), 'colspan' => 4, 'style' => 'text-align: right;'));
  $output .= theme('table', $header, $rows);

  return $output;
}
