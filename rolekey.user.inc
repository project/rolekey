<?php

/**
 * @file
 * Contains all hook_user() related functions.
 */

/**
 * Private function to show membership details.
 *
 * TODO: Make this actually reflect the role key memberships,
 * rather than simply the user roles.
 */
function _rolekey_user_memberships($account) {
  $memberships = array();
  $roles = array();
  $role_query = db_query("SELECT ur.rid, r.name FROM {users_roles} ur LEFT JOIN {role} r ON (ur.rid = r.rid) WHERE uid = %d", $account->uid);
  while ($row = db_fetch_object($role_query)) {
    $memberships[$row->rid] = $row->name;
  }
  return theme('rolekey_user_membership_notice', $memberships);
}

/**
 * @return array The registration role key
 */
function _rolekey_key_form_element() {
  $type = variable_get('rolekey_selection_type', 'textfield');
  if ($type == 'none') {
    return;
  }
  // only required if there are no empty key codes
  $required = ! (bool) db_result(db_query("SELECT count(*) FROM {rolekey_keys} WHERE rolekey ='' AND status = 1"));
  // only show if required or if there are more than 1 key to choose from
  $rolekeys = db_result(db_query("SELECT count(*) FROM {rolekey_keys} WHERE status = 1"));
  if ($rolekeys > 1 || $required) {
    $form['rolekey'] = array(
      '#title' => t('Registration codes'),
      '#type' => 'fieldset',
    );

    $form['rolekey']['rolekeycode'] = array(
      '#title' => t('Registration Code'),
      '#default_value' => '',
      '#required' => $required,
      '#description' => t('Please enter your registration code if you have one.'),
    );
    if ($type != 'textfield') {
      $options = array();
      foreach (rolekey_keys() as $kid => $key) $options[$kid] = t($key->name);
      $form['rolekey']['rolekeycode']['#options'] = $options;
    }
    switch ($type) {
      case 'textfield':
        $form['rolekey']['rolekeycode']['#type'] = 'textfield';
        break;
      case 'select':
        $form['rolekey']['rolekeycode']['#type'] = 'select';
        break;
      case 'radios':
        $form['rolekey']['rolekeycode']['#type'] = 'radios';
        break;
      case 'checkboxes':
        $form['rolekey']['rolekeycode']['#type'] = 'checkboxes';
        $form['rolekey']['rolekeycode']['#default_value'] = array();
        break;
    }
    return $form;
  }
}

/**
 * Validation callback for _rolekey_key_form_element.
 */
function _rolekey_key_form_element_validate($edit, $account, $category) {
  $type = variable_get('rolekey_selection_type', 'textfield');
  if (array_key_exists('rolekeycode', $edit) && $type != 'none') {
    if ($type == 'textfield') {
      $result = db_result(db_query("SELECT kid FROM {rolekey_keys} WHERE rolekey ='%s' AND status = 1", trim($edit['rolekeycode'])));
      if (!$result) {
        form_set_error('rolekeycode', t('An invalid registration code was entered.'));
      }
    }
    else {
      $keys = ($type == 'checkboxes') ? $edit['rolekeycode'] : array($edit['rolekeycode']);
      if (is_array($keys)) {
        foreach (array_filter($keys) as $key) {
          $result = db_result(db_query("SELECT kid FROM {rolekey_keys} WHERE kid =%d AND status = 1", $key));
          if (!$result) {
            form_set_error('rolekeycode', t('An invalid registration code was entered.'));
          }
        }
      }
    }
  }
}

/**
 * Submission callback for _rolekey_key_form_element.
 */
function _rolekey_key_form_element_submit(&$edit, $account, $category) {
  $type = variable_get('rolekey_selection_type', 'textfield');
  if (array_key_exists('rolekeycode', $edit) && $type != 'none') {
    if (variable_get('rolekey_show_rolekey_on_edit_form', 0) && !isset($edit['roles'])) {
      $result = db_query('SELECT r.rid, r.name FROM {role} r INNER JOIN {users_roles} ur ON ur.rid = r.rid WHERE ur.uid = %d', $account->uid);
      while ($role = db_fetch_object($result)) {
        $edit['roles'][$role->rid] = $role->rid;
      }
    }

    if ($type == 'textfield') {
      $result = db_query("SELECT DISTINCT rr.rid FROM {rolekey_roles} rr LEFT JOIN {rolekey_keys} rk ON (rk.kid = rr.kid) WHERE rk.rolekey ='%s' AND rk.status = 1", trim($edit['rolekeycode']));
      while ($role = db_fetch_object($result)) {
        $edit['roles'][$role->rid] = $role->rid;
      }
    }
    else {
      $keys = ($type == 'checkboxes') ? $edit['rolekeycode'] : array($edit['rolekeycode']);
      if (is_array($keys)) {
        $roles = array();
        foreach (array_filter($keys) as $key) {
          $result = db_query("SELECT rr.rid FROM {rolekey_roles} rr LEFT JOIN {rolekey_keys} rk ON (rk.kid = rr.kid) WHERE rk.kid ='%s' AND rk.status = 1", $key);
          while ($role = db_fetch_object($result)) {
            $edit['roles'][$role->rid] = $role->rid;
          }
        }
      }
    }
  }
}
