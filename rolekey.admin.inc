<?php

/**
 * @file
 * Contains all administration related functions.
 */

/**
 * Menu callback: administer role key settings.
 */
function rolekey_admin_settings() {
  $form['interface'] = array(
    '#type' => 'fieldset',
    '#title' => t('Registration interface settings'),
  );
  $form['interface']['rolekey_selection_type'] = array(
    '#type' => 'select',
    '#title' => t('Selection type'),
    '#default_value' => variable_get('rolekey_selection_type', 'textfield'),
    '#options' => _rolekey_selection_type_options(),
    '#description' => t('This option toogles how the user interacts with the process. If textfield is selected, the user must know the code to enter. If a select, radio or checkbox list is selected, the user can select any key(s) without having to know what the code is, (e.g: This gives unrestricted access to the role keys).'),
  );
  $form['user'] = array(
    '#type' => 'fieldset',
    '#title' => t('User interface settings'),
  );
  $form['user']['rolekey_show_rolekey_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show member info'),
    '#default_value' => variable_get('rolekey_show_rolekey_info', 0),
    '#description' => t('This option toogles the users "member" summary field in the "My account" area.'),
  );
  $form['user']['rolekey_show_rolekey_on_edit_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show role key on user edit form'),
    '#default_value' => variable_get('rolekey_show_rolekey_on_edit_form', 0),
    '#description' => t('This option toogles the role key in the edit "My account" area.'),
  );
  $form['#redirect'] = 'admin/user/rolekey';
  return system_settings_form($form);
}

/**
 * Helper function of allowed widget types for membership selection.
 */
function _rolekey_selection_type_options() {
  return array(
    'none' => t('Disable'),
    'textfield' => t('Textfield'),
    'select' => t('Select list'),
    'radios' => t('Radio buttons'),
    'checkboxes' => t('Check-boxes'),
  );
}

/**
 * Menu callback: display the custom edit form for no entry.
 */
function rolekey_list_keys_form(&$form_state) {
  $form = array();
  foreach (rolekey_keys(TRUE) as $kid => $key) {
    $form[$kid] = array(
      '#tree' => TRUE,
    );
// TODO: Work out a nice way to validate multiple keys
//       Keys must be unique! And the form values and database
//       keys will be out of sync. Currently easy, but paginating
//       would make this a bit more difficult.
//    $form[$kid]['rolekey'] = array(
//      '#type'           => 'textfield',
//      '#title'          => t('keyword'),
//      '#default_value'  => $key->rolekey,
//      '#size'           => 15,
//      '#maxlength'      => 127,
//    );
    $form[$kid]['status'] = array(
      '#type'           => 'checkbox',
      '#default_value'  => $key->status,
      '#disabled'       => ($kid == ROLEKEY_DEFAULT_KEY ? TRUE : FALSE),
    );
  }
  $form['submit'] = array(
    '#type'     => 'submit',
    '#value'    => t('Update keys'),
    '#validate' => array('rolekey_list_keys_form_validate'),
    '#submit'   => array('rolekey_list_keys_form_submit'),
  );

  return $form;
}

/**
 * Form rolekey_list_keys_form #validate callback.
 */
function rolekey_list_keys_form_validate($form, &$form_state) {
}

/**
 * Form rolekey_list_keys_form #submit callback.
 * For bulk editing of existing keys.
 */
function rolekey_list_keys_form_submit($form, &$form_state) {
  foreach (rolekey_keys(TRUE) as $kid => $key) {
    if (isset($form_state['values'][$kid])) {
      $values = $form_state['values'][$kid];
      $status = $kid == ROLEKEY_DEFAULT_KEY ? 1 : $values['status'];
      db_query("UPDATE {rolekey_keys} SET status = %d WHERE kid = %d", $status, $kid);
    }
  }
  drupal_set_message(t('Role keys have been updated.'));
  $form_state['redirect'] = 'admin/user/rolekey';
}

/**
 * Menu callback: administer role key forms.
 */
function rolekey_edit_key($form_state, $kid = NULL) {

  // Attempt to edit an existing key
  $key = new stdClass();
  if ($kid) {
    $key = db_fetch_object(db_query('SELECT * FROM {rolekey_keys} WHERE kid = %d', $kid));
    if (!$key->kid) {
      drupal_set_message(t('Invalid role key!'), 'error');
      drupal_goto('admin/user/rolekey');
    }
  }
  // Change title to current rolekey name
  if (isset($key->name)) {
    drupal_set_title(t("'%name' key", array('%name' => $key->name)));
  }

  // Build the form
  $form['kid'] = array(
    '#type'           => 'value',
    '#value'          => $key->kid
  );
  $form['name'] = array(
    '#type'           => 'textfield',
    '#title'          => t('key name'),
    '#default_value'  => $key->name,
    '#required'       => TRUE,
    '#size'           => 15,
    '#maxlength'      => 127,
    '#description'    => t('The name for this key. Example: "Gold member", "Seller".'),
  );
  $form['rolekey'] = array(
    '#type'           => 'textfield',
    '#title'          => t('keyword'),
    '#default_value'  => $key->rolekey,
    '#size'           => 15,
    '#maxlength'      => 127,
    '#description'    => t('The keyword that the user must enter. Example: "4ms03msTdUz6", "seller".'),
  );
  $form['status'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Enabled'),
    '#default_value'  => $key->status,
    '#disabled'       => ($kid == ROLEKEY_DEFAULT_KEY ? TRUE : FALSE),
    '#description'    => t('Use this option to disable the keyword from being used. Useful while your setting up your key roles or for disabling new subscriptions to this key.'),
  );

  // Build up the array of selected roles that are activated
  $result = db_query('SELECT * FROM {rolekey_roles} WHERE kid = %d', $key->kid);
  $selected_roles = array();
  while ($check = db_fetch_object($result)) {
    $selected_roles[$check->rid] = $check->rid;
  }
  $roles = user_roles(TRUE);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);
  if ($roles) {
    $form['rolekey_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Roles'),
      '#default_value' => $selected_roles,
      '#options' => $roles,
    );
  }

  // buttons are included in operations array
  $form['operations']['submit'] = array(
    '#type'     => 'submit',
    '#value'    => t('Save key'),
    '#weight'   => -10,
    '#validate' => array('rolekey_edit_key_validate'),
    '#submit'   => array('rolekey_edit_key_submit'),
  );
  if ($key->kid > ROLEKEY_DEFAULT_KEY) {
    $form['operations']['delete'] = array(
      '#type'     => 'submit',
      '#value'    => t('Delete'),
      '#submit'   => array('rolekey_delete_key_submit'),
    );
  }
  return $form;
}

/**
 * Form rolekey_edit_key #validate callback.
 */
function rolekey_edit_key_validate($form, &$form_state) {
  if (empty($form_state['values']['name'])) {
    form_set_error('name', t('You must specify a valid role key name.'));
  }
  if ($form_state['values']['kid']) {
    if (db_result(db_query("SELECT COUNT(*) FROM {rolekey_keys} WHERE name = '%s' AND kid <> %d", $form_state['values']['name'], $form_state['values']['kid']))) {
      form_set_error('name', t('The role key name %name already exists. Please choose another role key name.', array('%name' => $form_state['values']['name'])));
    }
    if (db_result(db_query("SELECT COUNT(*) FROM {rolekey_keys} WHERE rolekey = '%s' AND kid <> %d", $form_state['values']['rolekey'], $form_state['values']['kid']))) {
      form_set_error('rolekey', t('The role key %key already exists. Please choose another role key.', array('%key' => $form_state['values']['rolekey'])));
    }
  }
  else {
    if ($form_state['values']['name'] && db_result(db_query("SELECT COUNT(*) FROM {rolekey_keys} WHERE name = '%s'", $form_state['values']['name']))) {
      form_set_error('name', t('The role key name %name already exists. Please choose another role name.', array('%name' => $form_state['values']['name'])));
    }
    if ($form_state['values']['rolekey'] && db_result(db_query("SELECT COUNT(*) FROM {rolekey_keys} WHERE rolekey = '%s'", $form_state['values']['rolekey']))) {
      form_set_error('rolekey', t('The role key %key already exists. Please choose another role key.', array('%key' => $form_state['values']['rolekey'])));
    }
  }
}

/**
 * Form rolekey_edit_key
 * #submit callback for editing existing keys and associated roles.
 */
function rolekey_edit_key_submit($form, &$form_state) {
  $values = $form_state['values'];
  $status = ($values['kid'] == ROLEKEY_DEFAULT_KEY) ? 1 : $values['status'];

  $edit = array(
    'name'    => trim($values['name']),
    'rolekey' => trim($values['rolekey']),
    'status'  => $status,
  );

  if ($values['kid']) {
    $edit['kid'] = $values['kid'];
    drupal_write_record('rolekey_keys', $edit, array('kid'));
  }
  else {
    drupal_write_record('rolekey_keys', $edit);
  }

  // Update the enabled rolekeys
  $result = db_query('DELETE FROM {rolekey_roles} WHERE kid = %d', $edit['kid']);
  if (isset($values['rolekey_roles'])) {
    $active_rolekeys = array_filter($values['rolekey_roles']);
    foreach($active_rolekeys as $active_rolekey) {
      db_query('INSERT INTO {rolekey_roles} (kid, rid) VALUES (%d, %d)', $edit['kid'], $active_rolekey);
    }
  }

  drupal_set_message(t('The role has been updated.'));
  $form_state['redirect'] = 'admin/user/rolekey';
}

/**
 * Form rolekey_edit_key #submit callback for role key deletion.
 */
function rolekey_delete_key_submit($form, &$form_state) {
  if ($form_state['values']['kid'] == ROLEKEY_DEFAULT_KEY) {
    drupal_set_message(t('You can not delete the default registration key code.'), 'error');
  }
  else {
    db_query('DELETE FROM {rolekey_keys} WHERE kid = %d', $form_state['values']['kid']);
    db_query('DELETE FROM {rolekey_roles} WHERE kid = %d', $form_state['values']['kid']);
    drupal_set_message(t('The role key has been deleted.'));
  }
  $form_state['redirect'] = 'admin/user/rolekey';
}

/**
 * Menu callback: administer rolekey roles form.
 */
function rolekey_list_key_roles(&$form_state, $kid = NULL) {
  // query existing values
  $result = db_query('SELECT * FROM {rolekey_roles} rk');
  $rolekey_roles = array();
  while ($rolekey = db_fetch_object($result)) {
    $rolekey_roles[$rolekey->kid][] = $rolekey->rid;
  }
  // query role keys to edit
  $result = db_query('SELECT kid, name FROM {rolekey_keys} ORDER BY kid <> %d, name', ROLEKEY_DEFAULT_KEY);

  $keyrole_names = array();
  while ($rolekey = db_fetch_object($result)) {
    $keyrole_names[$rolekey->kid] = $rolekey->name;
  }
  // query roles
  $roles = array();
  $result = db_query('SELECT * FROM {role} WHERE rid <> %d AND rid <> %d ORDER BY name', DRUPAL_ANONYMOUS_RID, DRUPAL_AUTHENTICATED_RID);
  while ($role = db_fetch_object($result)) {
    $roles[$role->rid] = $role->name;
  }
  asort($roles);

  $options = array();
  $status = array();
  foreach ($roles as $rid => $role) {
    $options[$rid] = '';
    $form['roles'][$rid] = array('#value' => t($role));
    foreach ($keyrole_names as $kid => $name) {
      if (!array_key_exists($kid, $status)) {
        $status[$kid] = array();
      }
      // Builds arrays for checked boxes for each role key
      if ($rolekey_roles[$kid] && in_array($rid, $rolekey_roles[$kid])) {
        $status[$kid][] = $rid;
      }
    }
  }

  // Have to build checkboxes here after checkbox arrays are built
  foreach ($keyrole_names as $kid => $name) {
    $form['checkboxes'][$kid] = array('#type' => 'checkboxes', '#options' => $options, '#default_value' => $status[$kid]);
    $form['role_names'][$kid] = array('#value' => $name, '#tree' => TRUE);
  }
  // Remove warning in form.inc when only default roles exist
  if (!count($options)) {
    unset($form['checkboxes']);
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save roles'));
  return $form;
}

/**
 * Implementation of the forms hook_submit().
 */
function rolekey_list_key_roles_submit($form, &$form_state) {
  $result = db_query('SELECT * FROM {rolekey_keys}');

  while ($rolekey = db_fetch_object($result)) {
    if (isset($form_state['values'][$rolekey->kid])) {
      // Delete, so if we clear every checkbox we reset that role key;
      db_query('DELETE FROM {rolekey_roles} WHERE kid = %d', $rolekey->kid);
      $form_state['values'][$rolekey->kid] = array_filter($form_state['values'][$rolekey->kid]);
      foreach ($form_state['values'][$rolekey->kid] as $rid) {
        db_query("INSERT INTO {rolekey_roles} (rid, kid) VALUES (%d, %d)", $rid, $rolekey->kid);
      }
    }
  }

  drupal_set_message(t('The changes have been saved.'));
  $form_state['redirect'] = 'admin/user/rolekey';
}
